package chapter1;

import java.util.Scanner;

public class CompleteTheSentence {
    public static void main(String arg[]){
        //Get the season
        System.out.print("Season : ");
        Scanner scanner = new Scanner(System.in);
        String season = scanner.nextLine();

        //Get the adjective
        System.out.print("Adjective : ");
        String adjective = scanner.nextLine();

        //Get the whole number
        System.out.print("Number : ");
        int number = scanner.nextInt();

        //Complete the sentence
        System.out.print("On a " + adjective + " " + season + " day, I drink a minimum of " + number + " cups of coffee");

    }
}
