package chapter1;

import java.util.Scanner;

public class GrossPayCalculator {
    public static void main(String arg[]){
        //Get the numbers of hours worked
        System.out.print("Hours worked: ");
        Scanner scanner = new Scanner(System.in);
        int hours = scanner.nextInt();

        //Get the hourly pay rate
        System.out.print("Hourly pay rate: ");
        double rate = scanner.nextDouble();

        //Multiply hours and pay rate
        double grossPay = hours*rate;
        System.out.println("Gross Pay: $"+grossPay);

        //Display result
    }
}
