package chapter2;

/*
Logical Operator
To qualify for a loan, a person must make >= $30k
and have been working at their current job at least 2 years
 */

import java.util.Scanner;

public class LoanQualifier2 {
    public static void main (String args []) {
        int reqSalary = 30000;
        int reqWorkingYear = 2;

        //Get salary information
        System.out.print("Salary : ");
        Scanner scanner = new Scanner(System.in);
        double salary = scanner.nextDouble();

        //Get how many working year
        System.out.print("Working year : ");
        double year = scanner.nextDouble();

        //Loan decision
        if (salary >= reqSalary && year >= reqWorkingYear) {
                System.out.print("Qualified for loan");
        }
        else {
            System.out.print("Not qualified for loan, minimum salary is $" + reqSalary + " and minimum working for 2 years");
        }
    }
}
