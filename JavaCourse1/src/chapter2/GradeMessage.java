package chapter2;

import java.util.Scanner;

//Switch
public class GradeMessage {
    public static void main (String args []) {

        //Get letter grade
        System.out.print("Letter grade : ");
        Scanner scanner = new Scanner(System.in);
        String grade = scanner.next();

        String message;

        //Message decision
        switch (grade) {
            case "A":
                message = "Excellent job!";
                break;
            case "B":
                message = "Great job!";
                break;
            case "C":
                message = "Good job!";
                break;
            case "D":
                message = "Work harder";
                break;
            case "F":
                message = "Work much harder";
                break;
            default:
                message = "Invalid grade";
                break;
        }
        System.out.print(message);
    }
}
