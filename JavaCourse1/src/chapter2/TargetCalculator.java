package chapter2;

import java.util.Scanner;

/*
IF ELSE
All salespeople expected to make 10 sales per week
For those who do, they receive congratulations message
For those who don't, they are informed how many sales they were short
 */

public class TargetCalculator {
    public static void main(String args[]){
        int target = 10;

        //Get sales made this week
        System.out.print("Sales made : ");
        Scanner scanner = new Scanner(System.in);
        int sales = scanner.nextInt();

        //Decision will be made
        if (sales >= target) {
            System.out.print("Congrats, you've met your target!");
        } else {
            int salesShort = target - sales;
            System.out.print("You did not met your target, you were " + salesShort + " sales short");
        }

    }
}
