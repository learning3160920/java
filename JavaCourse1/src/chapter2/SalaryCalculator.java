package chapter2;

/*
IF Statement
All salespeople get a payment of $1000 a week.
Salespeople who exceed 10 sales get an additional bonus of $250
 */

import java.util.Scanner;

public class SalaryCalculator {
    public static void main(String args[]) {
        //Initialize known values
        int salary = 1000;
        int bonus = 250;
        int upLimit = 10;
        int downLimit = 5;

        //Get weekly sales
        System.out.print("Weekly sales : ");
        Scanner scanner = new Scanner(System.in);
        int sales = scanner.nextInt();
        scanner.close();

        //Bonus calculator
        if (sales > upLimit){
            salary = salary + bonus;
        } if (sales < downLimit){
            salary = salary - bonus;
        }
        System.out.print("Salary + bonus : " + salary);
    }
}
